**Director structure and file naming convention**

The area of Cologne has been covered with 9 tiles with zoom level "15".

The file naming scheme is as follows:

YYMMSDDhhmmss_LOC_IJXY.png

where

YY: two-digit year,

MM: month,

DD: day,

hh: hour in 24h-notation,

mm: minutes,

ss: seconds,

LOC: town name (here CGN),

I: index for the tile's rows (1-3),

J: index for the tile'S columns (1-3)

X: "W", "C", or "E" for west, center or east tile column,

Y: "N", "C", or "S" for north, center or south row.


`|  11WN  |  12CN  |  13EN  |`


`----------------------------`

`|  21WC  |  22CC  |  23EC  |`

`----------------------------`

`|  31WS  |  32CS  |  33ES  |`

